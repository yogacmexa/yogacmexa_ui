import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

import '@/assets/styles/index.css';

import Dialog from '@/functions/dialog/index.js';

const functions = {
  install: (Vue) => {
    Dialog.init(Vue);
  }
};



Vue.use(functions);

new Vue({
  render: h => h(App),
}).$mount('#app')
