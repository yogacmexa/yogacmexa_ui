import moment from 'moment';

moment.updateLocale('ru', {
    week: {
        dow: 1
    }
});

export const namesOfDays = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];
export const namesOfMonths = [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь'
];

export const now = moment();

export const generateDatesArray = (
    year = now.get('year'),
    month = now.get('month')
) => {
    const currentDate = moment([year, month]);
    const firstDayOfMonth = moment(currentDate)
        .startOf('month')
        .startOf('week')
        .set({
            hour: 0,
            minute: 0,
            second: 0,
            milisecond: 0
        });
    const lastDayOfMonth = moment(currentDate).endOf('month');
    const dates = [];
    for (let i = 0; i < 42; i++) {
        const clone = firstDayOfMonth.clone();
        if (moment(clone._d).isAfter(lastDayOfMonth, 'month')) {
            continue;
        } else if (moment(clone._d).isSame(currentDate, 'month')) {
            dates.push({
                value: clone._d,
                year: clone.get('year'),
                month: clone.get('month'),
                date: clone.get('date')
            });
        } else {
            dates.push(null);
        }

        firstDayOfMonth.add(1, 'day');
    }
    return dates;
};

export const formatDate = (date, format = 'DD.MM.YYYY') => {
    return moment(date).format(format);
};

export const fromNow = (date) => {
    return moment(date).fromNow();
};

export const duration = (startDate, endDate) => {
    const start = moment(startDate);
    const end = moment(endDate);
    return moment.duration(end.diff(start)).as('minutes');
};

export { moment };
