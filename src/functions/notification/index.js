import NotificationContainer from './notification-container';
import Notification from './notification';

export default {
    init: (Vue) => {
        if (Vue.prototype.$isServer) return;

        if (!Vue.prototype.$yui) {
            Vue.prototype.$yui = {};
        }

        const NotificationContainerContructor = Vue.extend(
            NotificationContainer
        );
        const notificationContainerInstance =
            new NotificationContainerContructor().$mount();
        document.body.appendChild(notificationContainerInstance.$el);

        Vue.prototype.$yui.notification = (propsData) => {
            const NotificationContructor = Vue.extend(Notification);
            const notificationInstance = new NotificationContructor({
                propsData
            }).$mount();
            notificationContainerInstance.$el.appendChild(
                notificationInstance.$el
            );
            return notificationInstance;
        };
    }
};
