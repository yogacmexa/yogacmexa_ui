import YAlert from '@/components/alert';
import YAvatar from '@/components/avatar';
import YButton from '@/components/button';
import YCard from '@/components/card';
import YCheckboxButton from '@/components/checkbox-button';
import YDatepicker from '@/components/datepicker';
import YDropdown from '@/components/dropdown';
import YDropdownItem from '@/components/dropdown/dropdown-item';
import YForm from '@/components/form';
import YFormItem from '@/components/form/form-item';
import YImageUpload from '@/components/image-upload';
import YInput from '@/components/input';
import YTabs from '@/components/tabs';
import YTabsItem from '@/components/tabs/tabs-item';
import YTag from '@/components/tag';
import YResult from '@/components/result';

/* Functions */
import FnDialog from '@/functions/dialog';
import FnNotification from '@/functions/notification';

/* Directives */
import clickOutside from './directives/click-outside';

import '@/assets/styles/index.css';

const components = [
    YAlert,
    YAvatar,
    YButton,
    YCard,
    YCheckboxButton,
    YDatepicker,
    YDropdown,
    YDropdownItem,
    YForm,
    YFormItem,
    YImageUpload,
    YInput,
    YTabs,
    YTabsItem,
    YTag,
    YResult
];

const functions = [FnDialog, FnNotification];

const install = (Vue) => {
    components.forEach((component) => {
        Vue.component(component.name, component);
    });

    if (Vue.prototype.$isServer) return;

    Vue.prototype.$yui = {};

    functions.forEach((fn) => {
        fn.init(Vue);
    });
};

export default {
    install,
    clickOutside,
    ...components
};
